const fs = require('fs');
const path = require('path');
const readline = require('readline');

function readConfigFileandgenerateTS() {
  try {
    const configFile = fs.readdirSync('./config/');

    configFile.forEach((file) => {
      const filePath = path.join('./config/', file);
      const fileStream = fs.createReadStream(filePath);
      const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity, // Recognize all instances of CR LF ('\r\n') as a single line break
      });

      rl.on('line', (line) => {
        generateJsonFromDirectory('.'+line.split('-')[1], line.split('-')[0]);
      });
      rl.on('close', () => {
        console.log('File reading completed.');
      });
    });

    console.table(configFile);
  } catch (error) {
    console.error(error);
  }
}

function generateJsonFromDirectory(directoryPath, filename) {
  const outputPath = './src/app/constant/auto/' + filename + '.constant.ts';
  try {
    const files = fs.readdirSync(directoryPath);
    const jsonResult = [];
    files.forEach(file => {
      const filePath = path.join(directoryPath, file);
      const stats = fs.statSync(filePath);
      if (stats.isFile()) {
        jsonPath = { displayText: '', filename: '', link: '' };
        console.log(file.split('.')[0])
        jsonPath.displayText = file.split('.')[0];
        jsonPath.filename = directoryPath.replace('/src','') + file;
        jsonResult.push(jsonPath);
      } else if (stats.isDirectory()) {
        // If it's a directory, recursively read its contents
        //jsonResult[file] = generateJsonFromDirectory(filePath);
      }
    });
    fs.writeFileSync(
      outputPath,
      'export const ' + filename + '=' + JSON.stringify(jsonResult,null, 2)
    );
    console.log(`JSON file generated at ${outputPath}`);
  } catch (error) {
    console.error('Error generating JSON file:', error);
  }
}

const outputPath = '';

readConfigFileandgenerateTS();

