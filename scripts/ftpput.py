import os
import ftplib
import hashlib
from datetime import datetime

# Configuration
HOSTNAME = 'ftp.example.com'
USERNAME = 'username'
PASSWORD = 'password'
DIST_DIR = './dir'
REMOTE_DIR = '/remote_folder'

# Connect to FTP server
ftp = ftplib.FTP(HOSTNAME, USERNAME, PASSWORD)
ftp.encoding = 'utf-8'

def md5_calculator(file_path):
    # Calculate the MD5 hash of a file
    hash_md5 = hashlib.md5()
    with open(file_path, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def list_local_files():
    # Get a list of files in the local directory with their MD5 hashes, including nested directories
    local_files = {}
    for root, _, files in os.walk(DIST_DIR):
        for file in files:
            file_path = os.path.join(root, file)
            relative_path = os.path.relpath(file_path, DIST_DIR)
            local_files[relative_path] = md5_calculator(file_path)
    return local_files

def fetch_remote_files():
    # Get a list of files in the remote directory with their sizes, including nested directories
    server_files = {}
    
    def list_files(dir_path):
        ftp.cwd(dir_path)
        file_list = []
        ftp.retrlines('LIST', file_list.append)
        
        for line in file_list:
            parts = line.split()
            file_name = ' '.join(parts[8:])
            if parts[0].startswith('-'):  # Regular file
                file_size = int(parts[4])
                server_files[os.path.join(dir_path, file_name).replace(REMOTE_DIR + '/', '')] = file_size
            elif parts[0].startswith('d'):  # Directory
                list_files(os.path.join(dir_path, file_name))
    
    list_files(REMOTE_DIR)
    return server_files

def upload_to_remote(local_path, remote_path):
    # Upload a file to the FTP server
    # with open(local_path, 'rb') as file:
        #ftp.storbinary(f'STOR {remote_path}', file)
    print(f"Upload # {local_path} -> {remote_path}")

def file_sync():
    # Sync local and remote directories.
    local_files = list_local_files()
    server_files = fetch_remote_files()

    # Upload new or modified files
    for file, md5 in local_files.items():
        remote_path = os.path.join(REMOTE_DIR, file).replace('\\', '/')
        local_path = os.path.join(DIST_DIR, file)
        local_size = os.path.getsize(local_path)
        if file not in server_files or server_files[file] != local_size:
            upload_to_remote(local_path, remote_path)

if __name__ == "__main__":
    try:
        file_sync()
    finally:
        ftp.quit()