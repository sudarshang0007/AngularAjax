#!/bin/bash

# Colors for output
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

# Function to log messages
log() { echo -e "${GREEN}[$(date '+%Y-%m-%d %H:%M:%S')] $1${NC}"; }
error() { echo -e "${RED}[$(date '+%Y-%m-%d %H:%M:%S')] ERROR: $1${NC}"; exit 1; }

# Check if local directory exists and is not empty
if [ ! -d "$LOCAL_DIR" ]; then error "Local directory '$LOCAL_DIR' does not exist!"; fi
if [  -z "$FTP_USER" ]; then error "Username:'$FTP_USER' does not exist!"; fi
if [  -z "$FTP_PASS" ]; then error "Password Not set!"; fi
if [ -z "$(ls -A "$LOCAL_DIR")" ]; then error "Local directory is empty. Nothing to sync."; fi

# Check if lftp is installed
if ! command -v lftp &> /dev/null; then
    error "lftp is not installed! Install it using 'sudo apt install lftp' or 'sudo yum install lftp'."
fi

# Start FTP sync
log "Starting FTP sync process..."

lftp -u "$FTP_USER","$FTP_PASS" "$FTP_HOST" <<EOF
set ftp:ssl-allow no
set ftp:use-feat no
set cmd:interactive no
set file:charset utf-8

mirror --reverse --delete --ignore-time --parallel=10 --verbose "$LOCAL_DIR" "$REMOTE_DIR"
exit
EOF

if [ $? -eq 0 ]; then
    log "FTP sync completed successfully!"
else
    error "FTP sync failed!"
fi
