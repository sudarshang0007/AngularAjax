export const important=[
  {
    "displayText": "Academic Calendar 2024-25",
    "filename": "./assets/documentation/imp/Academic Calendar 2024-25.pdf",
    "link": ""
  },
  {
    "displayText": "Academic Calendar 2025-26",
    "filename": "./assets/documentation/imp/Academic Calendar 2025-26.pdf",
    "link": ""
  },
  {
    "displayText": "PerspectivePlan2019-2024",
    "filename": "./assets/documentation/imp/PerspectivePlan2019-2024.pdf",
    "link": ""
  },
  {
    "displayText": "Prospectus",
    "filename": "./assets/documentation/imp/Prospectus.pdf",
    "link": ""
  }
]