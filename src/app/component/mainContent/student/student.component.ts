import { Component, OnInit } from '@angular/core';
import { student } from 'src/app/constant/auto/student.constant';
import { timetable } from 'src/app/constant/auto/timetable.constant';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  readonly studentList=student;
  readonly timetable=timetable;
  constructor() { }

  ngOnInit() {
  }

}
