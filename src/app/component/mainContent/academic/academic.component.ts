import { Component, OnInit } from '@angular/core';
import { academic } from 'src/app/constant/auto/academic.constant';

@Component({
  selector: 'app-academic',
  templateUrl: './academic.component.html',
  styleUrls: ['./academic.component.css']
})
export class AcademicComponent implements OnInit {

  readonly academicLinks= academic;
  constructor() { }

  ngOnInit() {
  }

}
