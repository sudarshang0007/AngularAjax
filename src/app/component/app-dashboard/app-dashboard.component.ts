import { Component } from '@angular/core';
import { importantLinks } from '../../constant/implinks.constant';
import { newsletter } from 'src/app/constant/auto/newsletter.constant';
import { academic } from 'src/app/constant/auto/academic.constant';
import { adevertise } from 'src/app/constant/auto/adevertise.constant';
import { important } from 'src/app/constant/auto/important.constant';

@Component({
  selector: 'app-dashboard',
  templateUrl: './app-dashboard.component.html',
  styleUrls: ['./app-dashboard.component.css']
})
export class AppDashboardComponent {
  Totaldate = { day: '', date: '', month: '', year: '', time: '' }

  readonly importantLinks=importantLinks;
  readonly important = important;
  readonly academicLinks = academic;
  readonly newsletter =newsletter;
  readonly advertiseList=adevertise;
  constructor() {
    important.forEach((element) => {importantLinks.push(element)});
  }
}
